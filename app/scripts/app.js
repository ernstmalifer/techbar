'use strict';

/**
 * @ngdoc overview
 * @name techbarApp
 * @description
 * # techbarApp
 *
 * Main module of the application.
 */
angular
  .module('techbarApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'angular.filter'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/classes');

    $stateProvider
    .state('main', {
      url: '/',
      views: {
        'bottomView': {
          templateUrl: 'views/topView.html',
          controller: 'TopViewCtrl'
        }
      }
    })
    .state('classes', {
      url: '/classes',
      views: {
        'bottomView': {
          templateUrl: 'views/bottomView.html',
          controller: 'BottomViewCtrl'
        }
      }
    })
    .state('slides', {
      url: '/slides',
      views: {
        'bottomView': {
          templateUrl: 'views/bottomViewSlides.html',
          controller: 'BottomViewSlidesCtrl'
        }
      }
    });

  });
