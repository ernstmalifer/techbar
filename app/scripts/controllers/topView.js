'use strict';

/**
 * @ngdoc function
 * @name techbarApp.controller:TopViewCtrl
 * @description
 * # TopViewCtrl
 * Controller of the techbarApp
 */
angular.module('techbarApp')
  .controller('TopViewCtrl', function ($scope, $interval, _, $timeout) {
    $scope.appointments = [
      {
        type: 'schedule',
        time: '10:30',
        classes: 'col-md-8',
        appointments: [
          { index: 0, name: 'David C.', descriptions: ['Mobile Device Setup', 'PC Tutorial'], expert: 'Kelly C.', expertid: 1, group: 1 },
          { index: 1, name: 'Fiona S.', descriptions: ['Mobile Device Tutorial'], expert: 'Martin S.', expertid: 2, group: 1 },
          { index: 2, name: 'Ben I.', descriptions: ['Mobile Device Tech Issue'], expert: 'Steph S.', expertid: 3, group: 1 },
          { index: 3, name: 'Belle P.', descriptions: ['Tech Bar Issue'], expert: 'Ernst M.', expertid: 4, group: 1 },
          { index: 4, name: 'Maggie R.', descriptions: ['Lab Issue'], expert: 'Don G.', expertid: 0, group: 1 },
          { index: 5, name: 'Third A.', descriptions: ['C.R. Allocation'], expert: 'Jetty B.', expertid: 0, group: 2 },
          { index: 6, name: 'Rachelle P.', descriptions: ['OMG Shoutout'], expert: 'Jean I.', expertid: 0, group: 2 }
        ]
      },
      {
        type: 'schedule',
        time: '11:00',
        classes: 'col-md-8',
        appointments: [
          { index: 0, name: 'David C.', descriptions: ['Mobile Device Setup', 'PC Tutorial'], expert: 'Kelly C.', expertid: 1, group: 1 },
          { index: 1, name: 'Fiona S.', descriptions: ['Mobile Device Tutorial'], expert: 'Martin S.', expertid: 2, group: 1 },
          { index: 2, name: 'Ben I.', descriptions: ['Mobile Device Tech Issue'], expert: 'Steph S.', expertid: 3, group: 1 },
          { index: 3, name: 'Belle P.', descriptions: ['Tech Bar Issue'], expert: 'Ernst M.', expertid: 4, group: 1 },
          { index: 4, name: 'Maggie R.', descriptions: ['Lab Issue'], expert: 'Don G.', expertid: 0, group: 1 },
          { index: 5, name: 'Third A.', descriptions: ['C.R. Allocation'], expert: 'Jetty B.', expertid: 0, group: 2 },
          { index: 6, name: 'Rachelle P.', descriptions: ['OMG Shoutout'], expert: 'Jean I.', expertid: 0, group: 2 }
        ]
      }
    ];

    $scope.active = 1;
    $scope.iclass = 1;
    $scope.moveTime = false;
    var iclassInterval;

    var iclassInterval = $interval(function() {
      $scope.iclass += 1;
    }, 300);

    var activeInterval = $interval(function(){
      $scope.iclass = 1;
      $interval.cancel(iclassInterval);

      if(2 == $scope.active) {
        $scope.active = 1;
      } else {
        $scope.active += 1;
      }

      iclassInterval = $interval(function() {
        $scope.iclass += 1;
      }, 300);

    }, 7000);

    var moveTimeInterval = $interval(function(){
      // switch time
      if($scope.moveTime) {
        $scope.moveTime = false;
      } else {
        $scope.moveTime = true;
      }
    }, 14000);

    var changeTimeout = $timeout(function(){

      $scope.removetech1 = 'remove';

      $timeout(function(){
        $scope.removetech2 = 'remove';
        $scope.removeappointments = 'remove2';
      }, 100)

      $timeout(function(){
        location.hash = '#/classes'
      }, 1500)

    }, 28000);

    $scope.$on('$locationChangeStart', function() {
      $interval.cancel(iclassInterval);
      $interval.cancel(moveTimeInterval);
      $interval.cancel(activeInterval);
      $timeout.cancel(changeTimeout);
    });

    $scope.$on('$viewContentLoaded', function(){
      // add 'remove' classes
      $scope.removetech1 = 'remove';
      $scope.removetech2 = 'remove';

      $scope.removeappointments = 'remove';

      $timeout(function(){
        $scope.removetech1 = '';
      }, 50)

      $timeout(function(){
        $scope.removetech2 = '';
        $scope.removeappointments = '';
      }, 100)

    });

  });
