'use strict';

/**
 * @ngdoc function
 * @name techbarApp.controller:BottomViewCtrl
 * @description
 * # BottomViewCtrl
 * Controller of the techbarApp
 */
angular.module('techbarApp')
  .controller('BottomViewCtrl', function ($scope, $interval, $timeout, _) {
    
    $scope.masterclasses = [
      {
        time: '10:30',
        begins: '15 mins',
        title: 'Fitter, Faster & Healthier',
        description: 'The latest tech for your health and wellbeing',
        id: 1,
        active: true,
        remove: ''
      },
      {
        time: '1:30',
        begins: '1 hr',
        title: 'Cloud Sync and Backup',
        description: 'Learn about the Cloud and secure your files',
        id: 2,
        active: false,
        remove: ''
      },
      {
        time: '3:00',
        begins: '2 hr',
        title: 'Online Safety for Your Family',
        description: 'Learn to identify and avoid online threats',
        id: 3,
        active: false,
        remove: ''
      }
    ];

    $scope.$on('$locationChangeStart', function() {
      $interval.cancel(switchInterval);
      $timeout.cancel(changeTimeout);
    });

    $scope.$on('$viewContentLoaded', function(){
      // add 'remove' classes
      $scope.removeCarousel = 'remove';
      $scope.removeclassh1 = 'remove';
      $scope.removeclasstimeh1 = 'remove';
      $scope.removeclasstimeh2 = 'remove';
      $scope.removeclassh2 = 'remove';
      $scope.masterclasses[0].remove = 'remove';
      $scope.masterclasses[1].remove = 'remove';
      $scope.masterclasses[2].remove = 'remove';

      // add 'remove' classes
      $timeout(function(){
        $scope.removeCarousel = '';
        $scope.removeclassh1 = '';
        $scope.removeclasstimeh1 = '';
      }, 50);

      $timeout(function(){
        $scope.removeclasstimeh2 = '';
        $scope.removeclassh2 = '';
        $scope.masterclasses[0].remove = '';
      }, 100)

      $timeout(function(){
        $scope.masterclasses[1].remove = '';
      }, 200)

      $timeout(function(){
        $scope.masterclasses[2].remove = '';
      }, 300)

    });

    $scope.active = 1;
    $scope.activeafter = '';

    $timeout(function(){
      $scope.activeafter = 'after'
    }, 1000)

    var switchInterval = $interval(function(){

      if($scope.masterclasses.length == $scope.active) {
        $scope.active = 1;
      } else {
        $scope.active += 1;
      }

      $scope.activeafter = '';
      $timeout(function(){
        $scope.activeafter = 'after'
      }, 1000)

      _.each($scope.masterclasses, function(carousel){
        if(carousel.id == $scope.active) {
          carousel.active = true;
        } else {
          carousel.active = false;
        }
      })

    }, 6000);


    var changeTimeout = $timeout(function(){

      $interval.cancel(switchInterval);
      $timeout.cancel(changeTimeout);

      // add 'remove' classes
      $scope.removeCarousel = 'remove';
      $scope.removeclassh1 = 'remove';
      $scope.removeclasstimeh1 = 'remove';

      $timeout(function(){
        $scope.removeclasstimeh2 = 'remove';
        $scope.removeclassh2 = 'remove';
        $scope.masterclasses[0].remove = 'remove';
      }, 100)

      $timeout(function(){
        $scope.masterclasses[1].remove = 'remove';
      }, 200)

      $timeout(function(){
        $scope.masterclasses[2].remove = 'remove';
      }, 300)

      $timeout(function(){
        location.hash = '#/slides'
      }, 1000)

      
    }, $scope.masterclasses.length * 6000);

  });
