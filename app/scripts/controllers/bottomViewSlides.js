'use strict';

/**
 * @ngdoc function
 * @name techbarApp.controller:BottomViewSlidesCtrl
 * @description
 * # BottomViewSlidesCtrl
 * Controller of the techbarApp
 */
angular.module('techbarApp')
  .controller('BottomViewSlidesCtrl', function ($scope, $interval, $timeout, $sce, _) {

    $scope.trustSrc = function(src) {
      return $sce.trustAsResourceUrl(src);
    }
    
    $scope.masterclasses = [
      { id: 0, src: 'Sc01.mp4', length: 15000 },
      { id: 1, src: 'Sc02.mp4', length: 11000 },
      { id: 2, src: 'Sc03.mp4', length: 23000 },
      { id: 3, src: 'Sc04.mp4', length: 19000 },
      { id: 4, src: 'Sc05.mp4', length: 18000 },
      { id: 5, src: 'Sc06.mp4', length: 15000 },
      { id: 6, src: 'Sc07.mp4', length: 12000 }
      // {
      //   title: 'The Next Masterclass<br>begins in <span>5 minutes</span>',
      //   description: 'Ask a team member for your complimentary coffee before it starts',
      //   image: '1.png',
      //   id: 1,
      //   active: true
      // },
      // {
      //   title: '',
      //   description: '',
      //   image: '2a.png',
      //   id: 2,
      //   active: false
      // },
      // {
      //   title: 'Tech Hiccups can<br>happen anytime',
      //   description: 'Platinum service is available 24/7. TO solve your tech issues,<br>call us anytime or Live Chat on Telsta.com',
      //   image: '2b.png',
      //   ask: 'Ask us how we can help.',
      //   id: 3,
      //   active: false
      // },
      // {
      //   title: 'Just got a<br>new device?',
      //   description: 'Enjoy free help getting started - we call it Walk Out Working.',
      //   image: '3a.png',
      //   ask: 'Ask a team member about what is included.',
      //   id: 4,
      //   active: false
      // },
      // {
      //   title: 'Just got a<br>new device?',
      //   description: 'Enjoy free help getting started - we call it Walk Out Working.',
      //   image: '3b.png',
      //   ask: 'Ask a team member about what is included.',
      //   id: 5,
      //   active: false
      // },
      // {
      //   title: 'Got a Gadget<br>Problem?',
      //   description: 'Everything from email setup and backup<br>to device repairs and life in the cloud.',
      //   image: '5a.png',
      //   ask: 'Just ask an Expert.',
      //   id: 6,
      //   active: false
      // },
      // {
      //   title: 'Got a Gadget<br>Problem?',
      //   description: 'Everything from email setup and backup<br>to device repairs and life in the cloud.',
      //   image: '5ba.png',
      //   ask: 'Just ask an Expert.',
      //   id: 7,
      //   active: false
      // },
      // {
      //   title: 'Got a Gadget<br>Problem?',
      //   description: 'Everything from email setup and backup<br>to device repairs and life in the cloud.',
      //   image: '5bb.png',
      //   ask: 'Just ask an Expert.',
      //   id: 8,
      //   active: false
      // },
      // {
      //   title: 'Got a Gadget<br>Problem?',
      //   description: 'Everything from email setup and backup<br>to device repairs and life in the cloud.',
      //   image: '5c.png',
      //   ask: 'Just ask an Expert.',
      //   id: 9,
      //   active: false
      // },
      // {
      //   title: 'Got a Gadget<br>Problem?',
      //   description: 'Everything from email setup and backup<br>to device repairs and life in the cloud.',
      //   image: '5d.png',
      //   ask: 'Just ask an Expert.',
      //   id: 10,
      //   active: false
      // },
      // {
      //   title: 'Help the<br>way you want it',
      //   description: 'Expert help from Telstra Platinum is available via the<br>phone, online, in store or we\'ll even visit you at home',
      //   image: '6a.png',
      //   ask: 'Ask a team member for more details.',
      //   id: 11,
      //   active: false
      // },
      // {
      //   title: 'Help the<br>way you want it',
      //   description: 'Expert help from Telstra Platinum is available via the<br>phone, online, in store or we\'ll even visit you at home',
      //   image: '6b.png',
      //   ask: 'Ask a team member for more details.',
      //   id: 12,
      //   active: false
      // },
      // {
      //   title: 'Help the<br>way you want it',
      //   description: 'Expert help from Telstra Platinum is available via the<br>phone, online, in store or we\'ll even visit you at home',
      //   image: '6c.png',
      //   ask: 'Ask a team member for more details.',
      //   id: 13,
      //   active: false
      // },
      // {
      //   title: 'Help the<br>way you want it',
      //   description: 'Expert help from Telstra Platinum is available via the<br>phone, online, in store or we\'ll even visit you at home',
      //   image: '6d.png',
      //   ask: 'Ask a team member for more details.',
      //   id: 14,
      //   active: false
      // },
      // {
      //   title: 'Need help around<br>your home?',
      //   description: 'From setting up your modem to creating a connected home -<br>a Platinum technician can help over the phone or come to you.',
      //   image: '7a.png',
      //   ask: 'Speak to a team member to learn more.',
      //   id: 15,
      //   active: false
      // },
      // {
      //   title: 'Need help around<br>your home?',
      //   description: 'From setting up your modem to creating a connected home -<br>a Platinum technician can help over the phone or come to you.',
      //   image: '7b.png',
      //   ask: 'Speak to a team member to learn more.',
      //   id: 16,
      //   active: false
      // },
      // {
      //   title: 'Need help around<br>your home?',
      //   description: 'From setting up your modem to creating a connected home -<br>a Platinum technician can help over the phone or come to you.',
      //   image: '7c.png',
      //   ask: 'Speak to a team member to learn more.',
      //   id: 17,
      //   active: false
      // },
      // {
      //   title: 'Help is on hand<br>with telstra platinum',
      //   description: 'Mobile help from $29 per service or from $15/mth on a Platinum Subscription*.',
      //   image: '8a.png',
      //   ask: 'Speak to a team member to learn more.',
      //   id: 18,
      //   active: false
      // },
      // {
      //   title: 'Help is on hand<br>with telstra platinum',
      //   description: 'PC help from $120 per service or from $15/mth on a Platinum subscription*',
      //   image: '8b.png',
      //   ask: 'Speak to a team member to learn more.',
      //   id: 19,
      //   active: false
      // }        
    ];

    $scope.$on('$locationChangeStart', function() {
      $interval.cancel(switchInterval);
      $timeout.cancel(changeTimeout);
    });


    $scope.active = 0;

    // var switchInterval = $interval(function(){
    //   if($scope.masterclasses.length == $scope.active) {
    //     $scope.active = 1;
    //   } else {
    //     $scope.active += 1;
    //   }

    //   _.each($scope.masterclasses, function(carousel){
    //     if(carousel.id == $scope.active) {
    //       carousel.active = true;
    //     } else {
    //       carousel.active = false;
    //     }
    //   })

    // }, 3000);

    // var switchInterval = $timeout(function(){
    //   $scope.active += 1;
    //   switchInterval = $timeout(function(){
    //     $scope.active += 1;
    //   }, $scope.masterclasses[$scope.active].length)
    // }, $scope.masterclasses[$scope.active].length)

    recur($scope.masterclasses[$scope.active].length)

    var changeTimeout = $timeout(function(){
      // location.hash = '#/'
    }, $scope.masterclasses.length * 3000);

    function recur(time){
      console.log($scope.active)

      var myChecker = $timeout(function () {

        $scope.active += 1;
        if ($scope.masterclasses.length == $scope.active) {
          // clearInterval(myChecker);
          location.hash = '#/'
        } else {
          recur($scope.masterclasses[$scope.active].length);
        }
      }, time);
    }

  });
